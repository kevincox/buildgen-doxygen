-- Copyright 2011 Kevin Cox

--[[---------------------------------------------------------------------------]
[                                                                              ]
[  Permission is hereby granted, free of charge, to any person obtaining a     ]
[  copy of this software and associated documentation files (the "Software"),  ]
[  to deal in the Software without restriction, including without limitation   ]
[  the rights to use, copy, modify, merge, publish, distribute, sublicense,    ]
[  and/or sell copies of the Software, and to permit persons to whom the       ]
[  Software is furnished to do so, subject to the following conditions:        ]
[                                                                              ]
[  The above copyright notice and this permission notice shall be included in  ]
[  all copies or substantial portions of the Software.                         ]
[                                                                              ]
[  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  ]
[  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    ]
[  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     ]
[  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  ]
[  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     ]
[  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         ]
[  DEALINGS IN THE SOFTWARE.                                                   ]
[                                                                              ]
[-----------------------------------------------------------------------------]]

S.import "stdlib"

L.doxygen = {}

if not P.L.doxygen then P.L.doxygen = {} end

do -- So that we can hide our locals.
local state = {}

--- Create new doxygen state
-- Creates and returns an opaque state.  This state is a table and is therefore
-- passed by refrence.
--
-- @return The newly created state.
function L.doxygen.newState ( )
	local data = {
		cmd = T.List(),
		format = {},
	}

	return data
end

--- Stashes the current state.
-- Returns the current state and loads a new state.  This is equivilent to
-- L.doxygen.swapState(L.doxygen.newState()).
--
-- @return The old state.
function L.doxygen.stashState ( )
	return L.doxygen.swapState(L.doxygen.newState())
end

--- Swap the state
-- Swaps new with the current state.
--
-- @param new The new state to load.
-- @return The old state.
function L.doxygen.swapState ( new )
	local old = state

	state = new

	return old
end

--- Load a state
-- Loads the state data
--
-- @param data The state to load.
function L.doxygen.loadState ( data )
	state = data
end

function addArg ( args )
	if type(args) ~= "table" then
		args = {tostring(args)}
	end
	state.cmd:extend(T.List(args))
end

--- Control what formats to generate.
--
-- @param format One of "html", "latex", "man", "rtf" or "man".
-- @param gen ##true## to enable to output format or ##false## to disable.
--          defaults to ##true##.
function L.doxygen.enableFormat ( format, gen )
	if gen == nil then gen = true end

	state.format[format] = gen
end

--- Define a variable
-- The variables passed will be defined during compilation.  If the value of a
-- key in map is not a string the key will just be defined.
--
-- This just calls S.cpp.define() with the state being used by doxygen.
--
-- @param map A table of key/value pairs to be defined.
function L.doxygen.define ( map )
	for k, v in pairs(map) do
		addArg(k.."="..v)
	end
end

--- Generate Documentation.
--
-- This generates the different forms into folders in the output dir.  The
-- ##OUTPUT_DIRECTORY## variable in the ##Doxyfile## is overwritten.  The
-- documentation formats generated is set by calls to L.doxygen.enableFormat.
-- These settings will override the ##GENERATE_*## variables in the Doxyfile
--
-- @param outdir The output directory to put the docs
-- @param out the executable to create..
-- @param inpuuts The dirctory in which the sources to be documented are located.
--          This will override the ##INPUT## variable.
-- @param options A table of options.  The following values are currently
--          supported.
--	<ul><li>
--		target: If set it will be used to create the target names.  To build all
--		  docs it will be ##$(target)## and for html (if enabled) it will be
--		  ##$(target)-html##.  This value defaults to "docs"
--	</li><li>
--		manfile: The name of the generated man pages are based upon the project
--		  and therefore we can't know a file to check for updates.  This means
--		  the man pages will always be rebuilt.  This option allows you to
--		  specify a file that will exist in your project so that we can add it
--		  as an output to allow proper dependancy-checked rebuilding.  This
--		  path is relative to the ##man/## directory in the documenation output
--		  directory.
-- </li><ul>
function L.doxygen.generate ( outdir, doxyfile, inputs, options )
	outdir   = C.path(outdir)
	doxyfile = C.path(doxyfile)
	if type(inputs) ~= "table" then
		inputs = {tostring(inputs)}
	end
	inputs   = T.List(inputs):map(C.path)
	local allinputs = T.List{doxyfile}
	for i in inputs:iter() do
		if T.path.isdir(i) then
			allinputs:extend(T.dir.getallfiles(i))
		else
			allinputs:append(i)
		end
	end

	options = options or {}
	options.target = options.target or "docs"

	local cmd = T.List{L.lualibsRoot .. "doxygen/gendoxyfile.lua", "Doxyfile", doxyfile}
	local formats = T.List();

	local importantFile = { -- Used as an output.
		html  = "html/index.html",
		latex = "latex/refman.tex",
		rtf   = "rtf/refman.rtf",
		man   = "man/",
		xml   = "xml/index.xml",
	}
	if options.manfile then
		importantFile.man = importantFile.man..options.manfile
	end

	for f in T.List{"html", "latex", "man",  "rtf", "xml"}:iter() do
		if options.prefix == "docs" then
			cmd[2] = ">Doxyfile-"..f
		else
			cmd[2] = ">Doxyfile-"..options.target.."-"..f
		end
		cmd[2] = C.path(cmd[2])

		cmd:extend(state.cmd)
		cmd:append('OUTPUT_DIRECTORY="'..outdir..'"')

		local ia = "INPUT="
		for i in inputs:iter() do
			ia = ia..'"'..i..'" '
		end
		ia = ia:sub(1, -2) -- Drop the last space.
		cmd:append(ia)

		if f == "html" then
			cmd:append "GENERATE_HTML=YES"
		else
			cmd:append "GENERATE_HTML=NO"
		end
		if f == "latex" then
			cmd:append "GENERATE_LATEX=YES"
		else
			cmd:append "GENERATE_LATEX=NO"
		end
		if f == "rtf" then
			cmd:append "GENERATE_RTF=YES"
		else
			cmd:append "GENERATE_RTF=NO"
		end
		if f == "man" then
			cmd:append "GENERATE_MAN=YES"
		else
			cmd:append "GENERATE_MAN=NO"
		end
		if f == "xml" then
			cmd:append "GENERATE_XML=YES"
		else
			cmd:append "GENERATE_XML=NO"
		end

		C.addGenerator({}, cmd, {cmd[2]}, {
			description = "Generating "..cmd[2]
		})
		C.addGenerator(T.List{cmd[2]}:extend(allinputs),
			{"*doxygen", cmd[2]},
			{outdir..importantFile[f]},
			{
				description = "Generating "..f.." docmunetation"
			}
		)
		S.addToTarget(options.target.."-"..f, outdir..importantFile[f])
		formats:append(options.target.."-"..f)

		cmd:chop(4) -- Remove the last arguments from the list.
	end

	for f in formats:iter() do
		if state.format[f] then -- Only add to the target if they want that format.
			S.addTargetToTarget(options.target, f)
		end
	end
end

L.doxygen.swapState(L.doxygen.newState())
end
